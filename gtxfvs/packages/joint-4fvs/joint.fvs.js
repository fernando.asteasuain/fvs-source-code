
/**************************************************************************
 * PAPER FVS
 */

// INPOT: Hack!! The only way so that Paper could have a pointermove event.
joint.dia.FVSPaper = joint.dia.Paper.extend({
    pointermove: function(evt) {

        evt.preventDefault();
        evt = joint.util.normalizeEvent(evt);

        var localPoint = this.snapToGrid({ x: evt.clientX, y: evt.clientY });

        if (this.sourceView) {
            // Mouse moved counter.
            this._mousemoved++;

            this.sourceView.pointermove(evt, localPoint.x, localPoint.y);
        } else {
            this.trigger('blank:pointermove', evt, localPoint.x, localPoint.y);
        }
    }
});


/**************************************************************************
 * SHAPES FVS
 */

joint.shapes.fvs = {};

//joint.shapes.fvs.State = joint.shapes.fsa.StartState;

joint.shapes.fvs.StateLabel = joint.dia.Element.extend({

    markup: '<g class="rotatable">' +
            '   <text/>' +
            '   <g class="scalable">' +
            '      <circle class="outer-circle hide"/>' +
            '      <circle class="middle-circle"/>' +
            '      <circle class="inner-circle"/>' +
            '   </g>' +
            '</g>',

    defaults: joint.util.deepSupplement({

        type: 'fvs.StateLabel',
        size: { width: 24, height: 24 },
        attrs: {
            '.outer-circle': {
                transform: 'translate(10, 10)',
                r: 18,
                fill: '#FFF',
                stroke: '#FFBB1E',
                'stroke-width': 3
            },
            '.middle-circle': {
                transform: 'translate(10, 10)',
                r: 15,
                fill: '#FFF',
            },
            '.inner-circle': {
                transform: 'translate(10, 10)',
                r: 12,
                fill: '#14c0cf',
                // http://stackoverflow.com/questions/31433647/how-do-i-add-a-drop-shadow-to-an-svg-path-element
                // http://www.w3schools.com/svg/svg_feoffset.asp
                // http://jointjs.com/tutorial/filters-gradients
                // filter: {
                //     name: 'dropShadow',
                //     args: {
                //         dx: 3,
                //         dy: 3,
                //         blur: 3,
                //         color: '#999'
                //     }
                // }
            },
            'text': {
                'font-size': 14,
                'text': '',
                'text-anchor': 'middle',
                'ref-x': .5,
                'ref-y': -15,
                'y-alignment': 'middle',
                'fill': '#000000',
                'font-family': 'Arial, helvetica, sans-serif'
            }
        }
    }, joint.dia.Element.prototype.defaults),

    _gtxfvs: {

        label: function(text) { // setter & getter
            if (text || text=="") { // act as setter
                //this.attributes.attrs.text.text = text;
                this.attr('text/text', text);
            }

            return this.attributes.attrs.text.text;
        },

        color: function(hexColor) {
            if (hexColor) {
                this.attr({
                    '.inner-circle': {
                        'fill': hexColor
                    }
                });
            }

            return this.attributes.attrs['.inner-circle']['fill'];
        }
    }
});

joint.shapes.fvs.Arrow = joint.shapes.fsa.Arrow.extend({

    defaults: joint.util.deepSupplement({
        type: 'fvs.Arrow',
        attrs: {
            '.outter-connection': {
                stroke: '#FFBB1E',
                'stroke-width': 3
            },
            '.connection': {
                stroke: '#000000',
                'stroke-width': 1
            },
            '.marker-source': {
                stroke: '#000000',
                fill: '#000000'
            },
            '.marker-target': {
                stroke: '#000000',
                fill: '#000000',
                d: 'M 10 0 L 0 5 L 10 10 z'
            }
        },
        smooth: true
    }, joint.dia.Link.prototype.defaults),

    _gtxfvs: {

        label: function(text) {
            if (text || text=="") { // act as setter
                this.label(0, { attrs: { 'text': {'text': text } } });
            }

            return this.attributes.labels[0].attrs.text.text;
        },

        color: function(hexColor) {
            if (hexColor) {
                this.attr({
                    '.connection': {
                        'stroke': hexColor
                    },
                    '.marker-source': {
                        'stroke': hexColor,
                        'fill': hexColor,
                    },
                    '.marker-target': {
                        'stroke': hexColor,
                        'fill': hexColor,
                    }
                });
            }

            return this.attributes.attrs['.connection']['stroke'];
        },

        smooth: function(smoothed) {
            if (smoothed != undefined && smoothed != null) {
                this.set('smooth', smoothed);
            }

            return this.get('smooth');
        },

        width: function(width) {
            if (width) {
                this.attr({
                    '.connection': {
                        'stroke-width': width
                    }
                });
            }

            return this.attributes.attrs['.connection']['stroke-width'];
        }
    }
});

joint.shapes.fvs.ImmediateNextArrow = joint.shapes.fvs.Arrow.extend({

    defaults: joint.util.deepSupplement({
        type: 'fvs.ImmediateNextArrow',
        attrs: {
            '.connection': { stroke: '#000000', 'stroke-width': 1 },
            '.marker-source': { stroke: '#000000', fill: '#000000' },
            '.marker-target': { stroke: '#000000', fill: '#000000',
                                d:  'M5.5,15.5,' +
                                    '16,21.5,' +
                                    '16,15.5,' +
                                    '20,15.5,' +
                                    '29.5,21.5,' +
                                    '20,15.5,' +
                                    '29.5,9.5,'  +
                                    '20,15.5,' +
                                    '16,15.5,' +
                                    '16,9.5 z'}
        },
        smooth: false
    }, joint.dia.Link.prototype.defaults)
});

joint.shapes.fvs.ImmediatePreviousArrow = joint.shapes.fvs.Arrow.extend({

    defaults: joint.util.deepSupplement({
        type: 'fvs.ImmediatePreviousArrow',
        attrs: {
            '.connection': { stroke: '#000000', 'stroke-width': 1 },
            '.marker-source': { stroke: '#000000', fill: '#000000',
                                d:  'M5.5,15.5,' +
                                    '20,15.5,' +
                                    '29.5,21.5,' +
                                    '20,15.5,' +
                                    '29.5,9.5,'  +
                                    '20,15.5 z'},
            '.marker-target': { stroke: '#000000', fill: '#000000', d: 'M 10 0 L 0 5 L 10 10 z' }
        },
        smooth: false
    }, joint.dia.Link.prototype.defaults)
});

