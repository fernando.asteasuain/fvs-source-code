#!/bin/bash

ln -s ../meteor-packages/lodash-4fvs/ packages/lodash-4fvs
ln -s ../meteor-packages/backbone-4fvs/ packages/backbone-4fvs
ln -s ../meteor-packages/joint-4fvs/ packages/joint-4fvs
ln -s ../meteor-packages/colorpicker-4fvs/ packages/colorpicker-4fvs
