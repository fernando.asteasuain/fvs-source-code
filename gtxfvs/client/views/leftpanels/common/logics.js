
ButtonRenderLogic = function(template, toolName) {

    var api = {
        render: render
    }

    return api;

    function render() {
        var container = $('#tools-container');
        Blaze.render(template, container.get(0));
        console.log(toolName + " initialized successfully!!");
    }
}

ToolLogic = function(PropertiesEditor, Drawer, tool) {

    console.log("Tool Logic for " + tool.name + " just created!");

    var drawer = null;
    var propertiesEditor = new PropertiesEditor(paper, $("#properties-container"));

    // and connect the event
    var button = $("#" + tool.id);
    button.on("click", function(e) {
        console.log(tool.name + " selected ... and ready to draw!!");

        drawer = new Drawer(paper, propertiesEditor);
        drawer.init();
    });

    button.bind('deactivated', function(){
        if(drawer != null) {
            drawer.stop();
            drawer = null;
        }
    });
}

Selector = new (function() {

    var api = {
        selectView: selectView,
        elemView: elemView,
        elemModel: elemModel,
        unselect: unselect
    };

    return api;

    function selectView(view) {
        Session.set('selectedCellViewId', "#" + view.id);
    }

    function elemView() {
        var view = null;

        var selectedCellViewId = Session.get('selectedCellViewId');
        if (selectedCellViewId) {
            view = paper.findView(selectedCellViewId);
        }
        return view;
    }

    function elemModel() {
        var view = elemView();
        if (view) {
            return graph.getCell(view.model.id);
        }
    }

    function unselect() {
        Session.set('selectedCellViewId', null);
    }
});