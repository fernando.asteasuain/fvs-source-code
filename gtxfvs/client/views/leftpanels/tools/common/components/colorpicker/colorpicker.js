
ColorPicker = function(options) {

    var model = {
        color: options.color || "AA00AA"
    }

    var colorPicker = null;

    // Template.colorpicker.onRendered(function() {
    //     colorPicker = this.$(".colorpicker-component").colorpicker();
    //     colorPicker.on('changeColor', options.onChangeColor);
    // });
    // Template.colorpicker.onDestroyed(function() {
    //     colorPicker.off('changeColor', options.onChangeColor);
    // });
    // Blaze.renderWithData(Template.colorpicker, model, options.domContainer);

    var view = Blaze.With(model, function() {
        return Template.colorpicker;
    });
    view.onViewReady(function() {
        var node = this.firstNode().parentNode;
        colorPicker = $(".colorpicker", node).colorpicker();
        colorPicker.on('changeColor', options.onChangeColor);
    });
    view.onViewDestroyed(function() {
        colorPicker.off('changeColor', options.onChangeColor);
    });

    Blaze.render(view, options.domContainer);
}
