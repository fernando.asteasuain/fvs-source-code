
StatePropertiesEditor = function(paper, propertiesContainer) {

    var view = null;
    var el = null;

    return {
        init: init,
        stop: stop
    }

    function init(elem) {
        console.log("PROPERTIES_EDITOR::State Initialized");
        console.log(elem);

        el = elem;
        // // save original attributes
        // el.attributes._attrs = {
        //     '.inner-circle': {
        //         // 'fill': el.attributes.attrs['.inner-circle']['fill'],
        //         // 'stroke': el.attributes.attrs['.inner-circle']['stroke'],
        //         // 'stroke-width': 0
        //     }
        // };

        // // set selection attributes
        // el.attr({
        //     '.inner-circle': {
        //         // 'fill': '#FFFF1E',
        //         // 'stroke': '#FFBB1E',
        //         // 'stroke-width': 3
        //     }
        // });

        //
        Session.set('idSelected', el.id);

        //
        var model = {
            // invoking label with call to set the context
            label: el._gtxfvs.label.call(el),
            color: el._gtxfvs.color.call(el)
        };
        view = Blaze.renderWithData(Template.toolStateProperties, model, propertiesContainer.get(0));
    }

    function stop() {
        console.log("PROPERTIES_EDITOR::State Stopped");

        // if (el) {
        //     // restore original attributes
        //     el.attr(el.attributes._attrs);
        //     el = null;
        // }

        // destroy template
        //propertiesContainer.empty();
        if (view) {
            Blaze.remove(view);
            view = null;
        }
    }
};
