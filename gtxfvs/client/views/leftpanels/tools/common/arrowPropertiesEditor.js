
PrecedenceArrowPropertiesEditor = function(paper, propertiesContainer) {
    return new ArrowPropertiesEditor(paper, $("#properties-container"), {
        init: "PROPERTIES_EDITOR::Precedence-Arrow Initialized",
        stop: "PROPERTIES_EDITOR::Precedence-Arrow Stopped"
    });
}

ImmediateNextArrowPropertiesEditor = function(paper, propertiesContainer) {
    return new ArrowPropertiesEditor(paper, $("#properties-container"), {
        init: "PROPERTIES_EDITOR::ImmediateNext-Arrow Initialized",
        stop: "PROPERTIES_EDITOR::ImmediateNext-Arrow Stopped"
    });
}

ImmediatePreviousArrowPropertiesEditor = function(paper, propertiesContainer) {
    return new ArrowPropertiesEditor(paper, $("#properties-container"), {
        init: "PROPERTIES_EDITOR::ImmediatePrevious-Arrow Initialized",
        stop: "PROPERTIES_EDITOR::ImmediatePrevious-Arrow Stopped"
    });
}

ArrowPropertiesEditor = function(paper, propertiesContainer, logTexts) {

    var view = null;
    var el = null;

    return {
        init: init,
        stop: stop
    }

    function init(elem) {
        console.log(logTexts.init);
        console.log(elem);

        el = elem;
        //
        Session.set('idSelected', el.id);

        // at the end: render properties template
        var model = {
            // invoking label with call to set the context
            label: el._gtxfvs.label.call(el),
            color: el._gtxfvs.color.call(el),
            smooth: el._gtxfvs.smooth.call(el),
            width: el._gtxfvs.width.call(el)
        };
        view = Blaze.renderWithData(Template.toolArrowProperties, model, propertiesContainer.get(0));
    }

    function stop() {
        console.log(logTexts.stop);

        // destroy template
        if (view) {
            Blaze.remove(view);
            view = null;
        }
    }
};