
var spanish = {
  navbar: {
    file: {
      id: 'Archivo',
      new: 'Nuevo',
      open: 'Abrir...',
      save: 'Guardar',
      saveAs: 'Guardar como...',
      downloadAs: 'Exportar como...',
      toJSON: 'Exportar como JSON',
      close: 'Cerrar',
    },
    edit: {
      id: 'Editar',
      redo: 'Rehacer',
      undo: 'Deshacer',
      clone: 'Clonar',
      delete: 'Eliminar'
    },
    search: 'Buscar...'
  },
  toolbar: {
    id: 'Herramientas'
  },
  properties: {
    id: 'Propiedades'
  },
  options: {
    id: 'Opciones'
  },
  footer: {
    signature: 'GTxFVS 2017. Featherweight Visual Scenarios al siguiente nivel!!'
  }
}

var english = {
  navbar: {
    file: {
        id: 'File',
        new: 'New',
        open: 'Open...',
        save: 'Save',
        saveAs: 'Save As...',
        downloadAs: 'Download As...',
        toJSON: 'Exportar as JSON',
        close: 'Close'
    },
    edit: {
      id: 'Edit',
      redo: 'Redo',
      undo: 'Undo',
      clone: 'Clone',
      delete: 'Delete'
    },
    search: 'Search...'
  },
  toolbar: {
    id: 'Toolbar'
  },
  properties: {
    id: 'Properties'
  },
  options: {
    id: 'Options'
  },
  footer: {
    signature: 'GTxFVS 2017. Featherweight Visual Scenarios to the next level!!'
  }
}

var languages = {
    'spanish': spanish,
    'english': english
};

Language = (function() {
    var api = {
        select: select,
        selected: selected
    };

    // default language.
    select('spanish');

    return api;

    function select(languageId) {
        var selected = languages[languageId];
        if(selected) {
            Session.set('selected-language', languageId);
        }
    }

    function selected() {
        return languages[Session.get('selected-language')];
    }
})();

Handlebars.registerHelper('language', function (textId) {
    var namespaces = textId.split(".");

    var lang = Language.selected();

    namespaces.forEach(function(namespace) {
        lang = lang[namespace];
    });

    return lang;
});
