Package.describe({
  name: 'lodash-4fvs',
  version: '3.10.1',
  summary: '',
  git: '',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.3.4.1');

  api.addFiles('lodash.js', 'client', { transpile: false });
});

Package.onTest(function(api) {

});
